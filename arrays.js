function distinctArr(array){
    return array.filter((element,index) => array.indexOf(element) === index);
}

function sumArr(array){
    return array.reduce((a, b) => a + b);
}

function intersect(array1,array2){
    return array1.filter(x => array2.includes(x))
}

function binSearch(arr, val) {
  let start = 0;
  let end = arr.length - 1;

  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    if (arr[mid] === val) {
      return mid;
    }
    if (val < arr[mid]) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }
  return -1;
}