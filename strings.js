function capitalize(str){
  return str.split(' ').map(x => (x[0].toUpperCase()) + x.substring(1)).join(' ');
}

function clean(str){
  return str.replace(/[^0-9A-Za-z]+/g, '')
}

function checkPalindrom(str){
    return str == str.split('').reverse().join('')
}

function distinct(str){
    let result = "";
    for(let i = 0; i < str.length; i++){
    if(result.includes(str[i]) == false){
      result += str[i]
      } 
    }
    return result;
}

function replaceAll(find, replace, str){
    return str.split(find).join(replace);
}