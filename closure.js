function CallCounter(){
    let start = 0;
    return function(){
        return start += 1;
    }
}
let counter = CallCounter();

console.log(counter());
console.log(counter());

function Sum(x){
    return function(y){
        return function(z){
            return x+y+z;
        }
    }
}
console.log(Sum(1)(2)(3));

function GenerateRandom(){
    let arr = [];
    return function(){
        if(arr.length == 100){
            return;
        }
        while(true){
            let number = Math.floor(1 + Math.random() * 100);
            if (!arr.includes(number)){
                arr.push(number)
                return number;
            }
        }
    }
}

let random = GenerateRandom();
for(let i = 1; i < 101; i++){
    console.log(random());
}

//фикс ловушки замыкания
const arr = [10, 12, 15, 21];
for (var i = 0; i < arr.length; i++) {
    let c = i;
    setTimeout(function() {
    console.log('Index: ' + c + ', element: ' + arr[c]);
    }, 3000);
}
