function zerosToEnd(arr){
    resultArr = [];
    zeroCount = 0;
    for(let i = 0; i < arr.length; i++){
        if(arr[i] == 0){
            zeroCount += 1;
        } else {
            resultArr.push(arr[i]);
        }
    }

    for(let k = 0; k < zeroCount; k++){
        resultArr.push(0);
    }

    return resultArr;
}

console.log(zerosToEnd([1,0,3,5,2,0,1]));