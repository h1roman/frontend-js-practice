function countMoney(sum){
    if(sum > 1500 || sum < 1) return -1;
    let nominales = [1000, 500, 200, 100, 50];
    let count = 0;
    for (let i = 0; i < nominales.length; i++) {
        while (sum - nominales[i] >= 0) {
            sum -= nominales[i];
            count++;
        }
    }
    return count;
}

console.log(countMoney(1250));